import sys
from bluepy.btle import Scanner
from socket import socket, AF_INET, SOCK_DGRAM

SERVER_IP   = '127.0.0.1' #'192.168.1.102'
PORT_NUMBER = 5000
SIZE = 1024
mySocket = socket( AF_INET, SOCK_DGRAM )

if __name__ == '__main__':

    while True:
        try:
            #Scaning for BLE devices 10 Hz
            ble_list = Scanner().scan(0.1)
            all_adres = ''

            for dev in ble_list:
                if dev.rssi > -55:
                    all_adres += dev.addr + ',' + str(dev.rssi) + ';'

            if all_adres != '':
                all_adres = all_adres[0:-1]
                print(all_adres)
            mySocket.sendto(all_adres.encode(),(SERVER_IP,PORT_NUMBER))

        except:
            raise Exception("Error occured")
            sys.exit()



