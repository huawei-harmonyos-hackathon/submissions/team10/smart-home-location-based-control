<!-- ABOUT THE PROJECT -->
## About The Project

We would like to build a smart system for managing  lights and speakers in house. 

We used Raspberry Pi's zero 2 to detect BLE tags that users would have with them. We will use tags for prototyping and conciser switching for detecting another Bluetooth devices (smartphones, watches etc.) later. Raspberry Pi zero 2 will send us BLE tag RSSI and ID. We will use ID for user identification and RSSI for localization. For localization, we will start by implementing zone localization, detecting that user is in some part of home because RSSI there is the strongest. Then we will start implementing triangulation localization method (for excellent accuracy),  using many receivers.

Audio and light 
We will use user localization for intelligent control of house appliances, staring with audio and light. The system, after detecting user movement, will turn on the light in the room user is now and turn off the light in the room user was in before.  We are also developing intelligent control for home audio system. Audio volume will be automatically  adjusted to keep volume experienced by the user at a constant level (going farther from speaker will make music/podcast louder).


Features:
* Playing user specific podcast/songs
* Turning light off if room is empty
* Pause podcast when user exit the room and play in the same spot it was paused when user enter the room again
* If there is more than one user in the room common music will be played



This project is currently nowhere near finished, so I'll be adding more features in the future. You may also suggest changes by forking this repo and creating a pull request or opening an issue.

### Built With
This section should list any major frameworks that you built your project using. Leave any add-ons/plugins for the acknowledgements section. Here are a few examples.
* [youtube-dl](https://pypi.org/project/youtube_dl/)
* [Pafy](https://pypi.org/project/pafy/)
* [python-vlc](https://pypi.org/project/python-vlc/)
* [Rich](https://pypi.org/project/rich/)
* [youtube_search_python](https://pypi.org/project/youtube-search-python/)
* [bluepy](https://github.com/IanHarvey/bluepy)




<!-- GETTING STARTED -->
## Getting Started


### Prerequisites

This script requires Python and VLC to run

### Installation

1. Clone the repo
```sh
git clone https://gitlab.com/groups/harmonyos-hackathon/submissions/team10
```

2. Install current released version
```sh
sudo apt-get install python-pip libglib2.0-dev
```
3. Install requirements.txt
```sh
sudo pip install -r requirements.txt
```
4. After new YT "feature" to disable view to dislike count you have to comment single line in 
youtube-dl code. The error will give you the path to the exact spot.
```sh
sudo nano /usr/local/lib/python3.8/dist-packages/pafy/backend_youtube_dl.py    
````

and comment line
```sh
self._dislikes = self._ydl_info['dislike_count']
```

<!-- USAGE EXAMPLES -->
## SETUP

You can now run bluepy_test.py which is our BlueTooth identification script it is also the server for socket communication.
```sh
sudo python3 bluepy_test.py 
```
Place your device for example phone or BLE-beacon near your RasberryPi.
Read the Bluetooth address of the device with the highest signal strength (the lowest RSSI),
repeat this procedure for all devices you would like to connect.
This script is also our socket sender service.


In server.py you have to configure some settings to your set up.
```sh
user = user_t("user_name", "BLE_address", playlist_number)
list_of_user = []
list_of_user.append(user)
```

To choose your music/podcast you have to copy the YouTube url address.
```sh
url_1= "https://www.youtube.com/watch?v=vzx6h2sAGTU"	
playlist_one = player.media_object_form_url(url_1)
playlista = [playlist_one]	#playlist_one number is 0
common_playlist_number = 0
number_of_songs = len(playlista)
```

For now one server per room is needed.
The option to have multi-room setup on one server is in development (multiprocessing is needed)
Make sure that your socket setup is the same as in bluepy_test.py
```sh
room_one = room_t('office', PORT_NUMBER, 'socket_IP_address', common_playlist_number)
rooms = []
rooms.append(room_one)
```

## USAGE

Start the bluepy_test.py
```sh
sudo python3 bluepy_server.py
```

To start the server device (connected to speakers and relay)
```sh
sudo python3 server.py
```





<!-- LICENSE -->
## 📝 License

Distributed under the Apache License 2.0. See `LICENSE` for more information.



<!-- CONTACT -->
## 📫 Contact

Jan Węgrzynowski - jjwegrzynowski@gmail.com
Grzegorz Czechmanowski - czechmanowski@gmail.com


<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
