from socket import socket, gethostbyname, AF_INET, SOCK_DGRAM
import statistics
import collections
import sys
import relay_control
from time import sleep
import play_music_test as player
from colors import bcolors

#### PLAYLIST INIT #####

url_podcast = "https://www.youtube.com/watch?v=vzx6h2sAGTU"
url = "https://www.youtube.com/watch?v=gCYcHz2k5x0"
url_podcast_2 = "https://www.youtube.com/watch?v=rN0gpd3Oyx0"

music = player.media_object_form_url(url)
podcast = player.media_object_form_url(url_podcast)
podacast_2 = player.media_object_form_url(url_podcast_2)

playlista = [music, podcast, podacast_2]
number_of_songs = len(playlista)
common_playlist = 0

# #	PRINT VIDEO DETAILS
print(f"\n{'-' * 70}\n")
print(bcolors.HEADER + "Now Playing: ")# + bcolors.OKCYAN + video.title)
print(bcolors.HEADER + "\nDuration: ")# + bcolors.OKCYAN + video.duration)

for user in range(number_of_songs):
    player.play_from_playlist(user, playlista)

sleep(10)

for user in range(number_of_songs):
    player.pause_from_playlist(user, playlista)

sleep(2)


####                    #####
PORT_NUMBER = 5000
SIZE = 1024
hostName = gethostbyname( '127.0.0.1' )

relay_control.init_GPIO()


class room_t():
    def __init__(self, name, PORT_NUMBER, hostName, wait_time):
        self.name = name
        self.users = set()
        self.number_of_users = 0
        self.users_time_dict = {}
        self.wait_time = wait_time * 10
        self.common_playlist_playing = False
        self.socket = socket(AF_INET, SOCK_DGRAM)
        self.socket.bind((gethostbyname( hostName ), PORT_NUMBER))
        print (name ,"server listening on port {0}\n".format(PORT_NUMBER))

    def add_user(self, name):
            self.users.add(name)
            self.users_time_dict[name] = self.wait_time 
            self.number_of_users = len(self.users)

    def remove_users(self):
        list_to_remove = []
        for user in self.users:
                print(self.users_time_dict)
                number = self.users_time_dict[user]
                if number == 0:
                        list_to_remove.append(user)
                else:
                        self.users_time_dict[user] = number-1

        for to_remove in list_to_remove:
                self.users.remove(to_remove)

        self.number_of_users = len(self.users)

    def ligts_control(self):
        if self.number_of_users > 0:
                relay_control.set_relay_1(14, True)
        else:
                relay_control.set_relay_1(14, False)

    def playlist_control(self, list_of_user):
            if self.number_of_users == 0:
                    print("stopoing playist - no users")
                    for user in list_of_user:
                            if user.playlist_playing:
                                    player.pause_from_playlist(user.playlist_number, playlista)
                                    user.stop_playlist()
            elif self.number_of_users == 1:
                    print("start playist - one users")
                    if self.common_playlist_playing:
                            player.pause_from_playlist(common_playlist, playlista)
                            self.common_playlist_playing = False

                    for user in list_of_user:
                            if not user.playlist_playing and user.name in self.users:
                                    player.play_from_playlist(user.playlist_number, playlista)
                                    user.start_playlist()
            else:
                    print("start playist - many users")
                    for user in list_of_user:
                            if user.playlist_playing:
                                    player.pause_from_playlist(user.playlist_number, playlista)
                                    user.stop_playlist()

                    if not self.common_playlist_playing:
                            player.play_from_playlist(common_playlist, playlista)
                            self.common_playlist_playing = True


class user_t:
    def __init__(self, name, addr, playlist_number):
            self.name = name
            self.addr = addr
            self.playlist_playing = False
            self.playlist_number = playlist_number
            self.rssi_buffer = collections.deque(maxlen=5)
    
    def get_avg_rssi(self):
        avg = statistics.mean(self.rssi_buffer)
        return round(avg)

    def get_name(self):
        return self.name

    def get_addr(self):
        return self.addr

    def update(self, rssi):
        self.rssi_buffer.append(rssi)

    def stop_playlist(self):
            self.playlist_playing = False

    def start_playlist(self):
            self.playlist_playing = True


def decode(mes, room):
        print(room.users)
        decode_mes = mes.decode()
        split_users = decode_mes.split(';')

        if len(mes) == 0:
                return 

        for user_data in split_users:
                adr, rssi_str = user_data.split(',')
                rssi = int(rssi_str)

                for user in list_of_user:
                        if adr == user.get_addr():
                                user.update(rssi)
                                if user.get_avg_rssi() > -70:
                                        room.add_user(user.name)



def control_loop(socket, room, list_of_user):
        while True:
                (mes,addr) = socket.recvfrom(SIZE)
                decode(mes, room)
                room.remove_users()
                room.ligts_control()
                room.playlist_control(list_of_user)
        sys.ext()



grzegorz = user_t("grzegorz", "7e:89:72:e7:41:c3", 2)
jan = user_t("jan", "60:14:78:76:37:0b", 1)

list_of_user = []
list_of_user.append(grzegorz)
list_of_user.append(jan)


room_one = room_t('office', 5000, '127.0.0.1', 1)

rooms = []
rooms.append(room_one)





for room in rooms:
	control_loop(room.socket, room, list_of_user)
