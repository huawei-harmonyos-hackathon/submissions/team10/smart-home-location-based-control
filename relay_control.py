import RPi.GPIO as GPIO

relay_pin_1 = 15
relay_pin_2 = 14

def init_GPIO():
    print("init_GPIO")
    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)
    GPIO.setup(relay_pin_1, GPIO.OUT, initial=GPIO.HIGH)
    GPIO.setup(relay_pin_2, GPIO.OUT, initial=GPIO.HIGH)

def set_relay_1(relay, state):
    if state:
        print("switch light one ON")
        GPIO.output(relay_pin_1, GPIO.LOW)
    else:
        print("switch light one OFF")
        GPIO.output(relay_pin_1, GPIO.HIGH)

def set_relay_2(relay, state):
    if state:
        print("switch 2 light one ON")
        GPIO.output(relay_pin_2, GPIO.LOW)
    else:
        print("switch 2 light one OFF")
        GPIO.output(relay_pin_2, GPIO.HIGH)

